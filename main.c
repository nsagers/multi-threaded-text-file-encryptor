/*
 * Nicholas Sagers
 * COM S 352: Project 2
 *
 * Implemented using a buffer queue and semaphores. All necessary encrypt.c
 * functions are utilized.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include "encrypt.h"

/* Buffer Queue */ 
typedef struct node node;
struct node {
	char c;
	int counted; // checks if the count thread has been here
	int encrypted; // checks if the encryption thread has been here
	node* prev;
};
typedef struct {
	node* head;
	node* tail;
	int capacity; // maximum size of the buffer queue
	int count; // current size of the buffer queue
} queue;

// Function prototypes
void* readInput(void* args);
void* countInput(void* args);
void* encryptInput(void* args);
void* countOutput(void* args);
void* writeOutput(void* args);
void displayCounts();
void debug(char* msg);

int bufSize; // Holds user buffer size input

queue input_bufferq; // buffer for the input string
queue output_bufferq; // buffer for the output string

// semaphores to handle race conditions
sem_t read_in;
sem_t count_in;
sem_t encrypt_in;
sem_t encrypt_out;
sem_t count_out;
sem_t write_out;

int debugging = 0; // activates some print statements that may or may not be useful

int main(int argc, char** argv) {
	pthread_t in, icount, en, ocount, out;

	if ( argc != 3 ) {
                printf("Usage: ./encrypt352 input output\n");
		exit(0);
	}

        open_input(argv[1]);
        open_output(argv[2]);
	
        // Code to construct the buffers
	printf("Enter Buffer Size:");
	fflush(stdout);	
	char bufSizeReader[256];
	fgets(bufSizeReader, 256, stdin);
	bufSizeReader[strlen(bufSizeReader) - 1] = '\0';
	bufSize = atoi(bufSizeReader);
	input_bufferq.capacity = bufSize;
	input_bufferq.count = 0;
	output_bufferq.capacity = bufSize;
	output_bufferq.count = 0;
	
        // semaphore initialization
	sem_init(&read_in, 0, 1);
	sem_init(&count_in, 0, 0);
	sem_init(&encrypt_in, 0, 0);
	sem_init(&encrypt_out, 0, 1);
	sem_init(&count_out, 0, 0);	
	sem_init(&write_out, 0, 0);
	
        // thread initialization
	pthread_create(&in, NULL, readInput, NULL);
	pthread_create(&icount, NULL, countInput, NULL);
	pthread_create(&en, NULL, encryptInput, NULL);
	pthread_create(&ocount, NULL, countOutput, NULL);
	pthread_create(&out, NULL, writeOutput, NULL);
	
        // wait for thread completion
	pthread_join(in, NULL);
	pthread_join(icount, NULL);
	pthread_join(en, NULL);
	pthread_join(ocount, NULL);
	pthread_join(out, NULL);
        
        printf("End of file reached\n");
	return 1;
}

// for buffer queue
int enqueue(queue* q, char c){
	if ( q->count == q->capacity )	return 0;
	
	node* newChar = (node*) malloc(sizeof(node));
	newChar->counted = 0;
	newChar->encrypted = 0;
	newChar->c = c;
	
	if ( q->count == 0) {
		q->head = newChar;
		q->tail = newChar;
	}
       
        else {
		q->tail->prev = newChar;
		q->tail = newChar;
	}

	q->count = q->count + 1;
	return 1;
}

// for buffer queue
node* dequeue(queue* q){
	if ( q->count == 0 )
		return (node*)NULL;
	
	node* curNode = (node*) malloc(sizeof(node));
	curNode->c = q->head->c;
	curNode->counted = q->head->counted;
	curNode->encrypted = q->head->encrypted;
	q->head = q->head->prev;

	if ( q->count == 1 ) 
		q->tail = NULL;
	
	q->count--;
	return curNode;
}

// Encryption thread: utilizes caesar_encrypt
void* encryptInput(void* args){
	node* curIn;
	node* temp;
	int wasProcessed = 1;

	while ( 1 ) {
		sem_wait(&encrypt_in);
		curIn = input_bufferq.head;
		while ( curIn != NULL ) {
			if ( curIn->counted && !curIn->encrypted) {
				if ( curIn->c != EOF && curIn->c != '\n' ) {
					curIn->c = caesar_encrypt(curIn->c); 	
				}
				curIn->encrypted = 1;
				break;
			}
			curIn = curIn->prev;
		}
		
		if (input_bufferq.count > 0 && input_bufferq.head->encrypted){
			temp = dequeue(&input_bufferq);
			sem_post(&read_in);
		}

		sem_wait(&encrypt_out);
		enqueue(&output_bufferq, temp->c);
		sem_post(&count_out);

		if ( temp->c == EOF ) {
			break;
		}
	}
}

// output counter thread: utilizes count_output
void* countOutput(void* args){
	int i;
	node* cur;
	
	while ( 1 ) {
		sem_wait(&count_out);
		cur = output_bufferq.head;
		while ( cur != NULL ) {
			if ( !cur->counted ) {
                                count_output(cur->c);
				cur->counted = 1;
				sem_post(&write_out);
				if ( cur->c == EOF ) {
					return (void*) NULL;
				}
                                else {
					break;
				}
			} else {
				cur = cur->prev;
			}
		}
	}
}

// Input counter thread: utilizes count_input
void* countInput(void* args){
	int i;
	node* cur;

	while ( 1 ) {
		sem_wait(&count_in);
		cur = input_bufferq.head;
		while ( cur != NULL ) {
			if ( cur->counted == 0 ) {
                                count_input(cur->c);
				cur->counted = 1;
				sem_post(&encrypt_in);
				
				if ( cur->c == EOF ) {
					return (void*) NULL;
				} else {
					break;					
				}
			} else {
				cur = cur->prev;
			}	
		}

	}
	
}

// Reader thread: utilizes read_input
void* readInput(void* args){
	char cur;
        cur = read_input();
	
	while ( 1 ) {
		sem_wait(&read_in);
		if ( enqueue(&input_bufferq, cur) ) {
			sem_post(&count_in);
			if ( cur == EOF) {
				break;
			} else {
                                cur = read_input();
			}
		}
		
	}
}

// Writer thread: utilizes write_output
void* writeOutput(void* args){
	node* cur;

	while ( 1 ) {
		sem_wait(&write_out);
		cur = output_bufferq.head;

		if ( cur->counted ) {
			dequeue(&output_bufferq);
			
			if ( cur->c == EOF ) {
				break;
			}
			
                        write_output(cur->c);
			cur = cur->prev;
		} 
		sem_post(&encrypt_out);
	}
}

// for figuring out how it all went wrong
void debug(char* msg){
	if ( debugging ) {
		printf("%s", msg);
	}
}

// uses the same semaphore as encryptInput such that we don't mess up an
// in progress encryption
void reset_requested() {
    debug("request started\n");
    sem_wait(&encrypt_in);
    displayCounts();
}

void reset_finished() {
    printf("Reset finished\n");
    sem_post(&encrypt_in); // now we are free to start encrypting again
}

// prints similarly to the example in the project 2 pdf
// prints the frequency of all 26 letters in both the input and output
void displayCounts() {
    int i;

    printf("\nInput file contains\n");
    for (i = 65; i < 91; i++) {
        if (i != '\n')
            printf("%c:%d ", i, get_input_count(i));
    }

    printf("\n\nOutput file contains\n");
    for (i = 65; i < 91; i++) {
        if (i != '\n')
            printf("%c:%d ", i, get_output_count(i));
    }
    printf("\n");
}
